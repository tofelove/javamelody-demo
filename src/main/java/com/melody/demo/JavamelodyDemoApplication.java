package com.melody.demo;

import com.melody.demo.service.HelloService;
import net.bull.javamelody.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class JavamelodyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavamelodyDemoApplication.class, args);
    }

    @Autowired
    private HelloService helloService;

    @RequestMapping("/")
    public String hello() {
        String hello = helloService.hello();
        return hello;
    }

    @RequestMapping("/err")
    public String error() {
        throw new RuntimeException("error");
    }

    @RequestMapping("/para")
    public String javaMelodyParameter() {
        //throw new RuntimeException("error");
        Parameter[] values = Parameter.values();
        for (Parameter parameter : values) {
            System.out.println(parameter.getCode() + " --> "+ parameter.getValue());
        }

        System.out.println(System.getProperties());

        return "ok";

    }

}
