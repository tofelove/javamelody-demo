package com.melody.demo.service;

import org.springframework.stereotype.Service;

/**
 * ${DESCRIPTION}
 *
 * @author sxp
 * @create at 2018/8/18.11:47
 */
@Service
public class HelloService {

    private String extra;

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getExtra() {
        return extra;
    }

    public String hello() {
        System.out.println("proceed hello");
        return "hello world," + extra;
    }

}
