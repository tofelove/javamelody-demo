/*
 * Copyright 2008-2017 by Emeric Vernat
 *
 *     This file is part of Java Melody.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.melody.demo.config.beanprocessor;

import com.melody.demo.service.HelloService;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;


public class HelloBeanPostProcessor implements BeanPostProcessor, PriorityOrdered {

	private String extra;

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getExtra() {
		return extra;
	}

	public HelloBeanPostProcessor(String extra){
		this.extra=extra;
	}

	private int order = LOWEST_PRECEDENCE;

	@Override
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	/** {@inheritDoc} */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) {
		return bean;
	}

	/** {@inheritDoc} */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {
		// RestTemplate et getInterceptors() existent depuis spring-web 3.1.0.RELEASE
		if ( bean instanceof HelloService) {
			final HelloService helloService = (HelloService) bean;
            helloService.setExtra(extra);
		}

		return bean;
	}


}
