package com.melody.demo.config.aop;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;

/**
 * pointcut 切点
 * advice   切面
 * <p>
 * advisor  切点+切面
 */
public class HelloSpringAdvisor extends DefaultPointcutAdvisor {
    private static final long serialVersionUID = 1241977525834332907L;


    public HelloSpringAdvisor() {

    }

    /**
     * Constructeur.
     *
     * @param pointcut Pointcut
     */
    public HelloSpringAdvisor(Pointcut pointcut, Advice advice) {
        super();
        setAdvice(advice);
        setPointcut(pointcut);

        // ordre par rapport aux autres advisors/aspects (issue 32)
        setOrder(100);
    }
}