package com.melody.demo.config;

import com.melody.demo.config.aop.HelloSpringAdvisor;
import com.melody.demo.config.aop.helloSpringAspect;
import com.melody.demo.config.beanprocessor.HelloBeanPostProcessor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 *
 *
 * @author sxp
 * @create at 2018/8/18.11:32
 */
@Configuration
public class HelloConfig {

    /**
     * Monitoring of beans having the {@link Service} annotation.
     * @return HelloSpringAdvisor
     */
    @Bean
    public HelloSpringAdvisor helloServiceAdvisor() {
        return new HelloSpringAdvisor(new AnnotationMatchingPointcut(Service.class), new helloSpringAspect());
    }

    @Bean
    public HelloBeanPostProcessor helloBeanPostProcessor() {
        return new HelloBeanPostProcessor("bean post process add extra!");
    }


}
